package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

//Person struct
type Person struct {

	//ID do tripulante
	ID int `json:"id" db:"ID"`

	//Survived do tripulante
	Survived int `json:"survived" db:"Survived"`

	//Pclass do tripulante
	Pclass int `json:"passengerClass" db:"Pclass"`

	//Name do tripulante
	Name string `json:"name" db:"Name"`

	//Sex do tripulante
	Sex string `json:"sex" db:"Sex"`

	//Age do tripulante
	Age int `json:"age" db:"Age"`

	//Siblings do tripulante
	Siblings int `json:"siblingsOrSpousesAboard" db:"Siblings/Spouses Aboard"`

	//Parents do tripulante
	Parents int `json:"parentsOrChildrenAboard" db:"Parents/Children Aboard"`

	//Fare do tripulante
	Fare string `json:"fare" db:"Fare"`
}

var db *sql.DB
var err error

func main() {
	db, err = sql.Open("mysql", "root:25ago08R@tcp(database-titanic:3306)/dados")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	router := mux.NewRouter()

	router.HandleFunc("/people", getPeople).Methods("GET")
	router.HandleFunc("/people", createPerson).Methods("POST")
	router.HandleFunc("/people/{id}", getPerson).Methods("GET")
	router.HandleFunc("/people/{id}", updatePerson).Methods("PUT")
	router.HandleFunc("/people/{id}", deletePerson).Methods("DELETE")

	http.ListenAndServe(":8000", router)
}

func getPeople(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var persons []Person
	result, err := db.Query("SELECT ID, Survived, Pclass, Name, Sex, Age, `Siblings/Spouses Aboard`, `Parents/Children Aboard`, Fare from titanic")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var person Person
		err := result.Scan(&person.ID, &person.Survived, &person.Pclass, &person.Name, &person.Sex, &person.Age, &person.Siblings, &person.Parents, &person.Fare)
		if err != nil {
			panic(err.Error())
		}
		persons = append(persons, person)
	}
	json.NewEncoder(w).Encode(persons)
}
func createPerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	stmt, err := db.Prepare("INSERT INTO titanic(ID, Survived, Pclass, Name, Sex, Age, `Siblings/Spouses Aboard`, `Parents/Children Aboard`, Fare) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	keyVal := make(map[string]string)
	keyVal1 := make(map[string]int)
	json.Unmarshal(body, &keyVal)
	json.Unmarshal(body, &keyVal1)
	id := keyVal1["id"]
	survived := keyVal1["survived"]
	passengerClass := keyVal1["passengerClass"]
	name := keyVal["name"]
	sex := keyVal["sex"]
	age := keyVal1["age"]
	siblingsOrSpousesAboard := keyVal1["siblingsOrSpousesAboard"]
	parentsOrChildrenAboard := keyVal1["parentsOrChildrenAboard"]
	fare := keyVal1["fare"]
	_, err = stmt.Exec(id, survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "New person was created")
}
func getPerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	result, err := db.Query("SELECT ID, Survived, Pclass, Name, Sex, Age, `Siblings/Spouses Aboard`, `Parents/Children Aboard`, Fare from titanic WHERE ID = ?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	var person Person
	for result.Next() {
		err := result.Scan(&person.ID, &person.Survived, &person.Pclass, &person.Name, &person.Sex, &person.Age, &person.Siblings, &person.Parents, &person.Fare)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(person)
}
func updatePerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	stmt, err := db.Prepare("UPDATE titanic SET ID = ?,  Survived = ?,  Pclass = ?,  Name = ?,  Sex = ?,  Age = ?,  `Siblings/Spouses Aboard` = ?,  `Parents/Children Aboard` = ?,  Fare = ? WHERE ID = ?")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	keyVal := make(map[string]string)
	keyVal1 := make(map[string]int)
	json.Unmarshal(body, &keyVal)
	json.Unmarshal(body, &keyVal1)
	id := keyVal1["id"]
	survived := keyVal1["survived"]
	passengerClass := keyVal1["passengerClass"]
	name := keyVal["name"]
	sex := keyVal["sex"]
	age := keyVal1["age"]
	siblingsOrSpousesAboard := keyVal1["siblingsOrSpousesAboard"]
	parentsOrChildrenAboard := keyVal1["parentsOrChildrenAboard"]
	fare := keyVal1["fare"]
	_, err = stmt.Exec(id, survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare, params["id"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "Person with id = %s was updated", params["id"])
}
func deletePerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	stmt, err := db.Prepare("DELETE FROM titanic WHERE ID = ?")
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(params["id"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "id = %s was deleted", params["id"])
}
